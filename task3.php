<?php
/*
Ligonio temperatūra per parą matuojama kas valandą arba kas kelias valandas ir užrašoma į ligonio kortelę.
Parašykite programą, kuri rastų:
•	Kurią valandą temperatūra buvo aukščiausia
•	Kuriomis valandomis temperatūra buvo artima (+/- 0.5 laipsnio) aukščiausiai temperatūrai.
•	Kuriomis valandomis temperatūra buvo gera nuo 36.40 iki 37.00
Turimi du įvedimo laukai. Pirmame lauke įvedamos matavimo valandos atskirtos kableliais, antrame įvedamos temperatūros atskirtos taip pat kableliais.
Rezultatas:
Auksciausia temperatura: 39.7 buvo 16 val.
Auksciausia temperatura dar buvo:
16 val. 39.5
Normali temperatūra buvo:
17 val. 36.7
*/ ?>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
</head>

<body>   

<br>
<h1 align="center">6.3 užduotis</h1>
<br>

<form method="GET">
<div class="form-group row">
  <label for="example-text-input" class="col-4 col-form-label" align="center">Įveskite temperatūros matavimo valandas:</label>
  <div class="col-4">
    <input class="form-control" type="text" name="hours" value=" " id="example-text-input">    
  </div>  
  
</div>
<div class="form-group row">
  <label for="example-text-input" class="col-4 col-form-label" align="center">Įveskite temperatūrų duomenis:</label>
  <div class="col-4">
    <input class="form-control" type="text" name="temperatures" value=" " id="example-text-input">    
  </div>
  <button type="submit" class="btn btn-primary">Patvirtinti</button>
</div>
</form>
<?php 
    var_dump($_GET['hours']);
    var_dump($_GET['temperatures']);
    echo '<br>'?>

<?php

if (!empty($_GET['hours'])) {
    $hoursArray = explode(', ', $_GET['hours']);
}
if (!empty($_GET['temperatures'])) {
    $temperatureArray = explode(', ', $_GET['temperatures']);
}
    $hoursAndTemperaturesArray = array_combine($hoursArray, $temperatureArray);  
    $sortedArray = asort($hoursAndTemperaturesArray);

    $maxTemperature = end($hoursAndTemperaturesArray);    
    $hourOfMaxTemperature = array_keys($hoursAndTemperaturesArray, $maxTemperature);   
    echo 'Aukščiausia temperatūra: ' . $maxTemperature . ' buvo ' . $hourOfMaxTemperature[0] . ' valandą. <br>';   

    foreach ($hoursAndTemperaturesArray as $hour => $temperature) {
        if (($maxTemperature - $temperature) > 0 && ($maxTemperature - $temperature) <= 0.5) {      
            echo 'Aukščiausia temperatūra dar buvo: ' . $temperature . ' ' . $hour . ' valandą. <br>';
        } 
    }

    foreach ($hoursAndTemperaturesArray as $hour => $temperature) {
        if ($temperature > 36.40 && $temperature < 37) {      
            echo 'Normali temperatūra buvo: ' . $temperature . ' ' . $hour . ' valandą. <br>';
        } 
    }
      
?>
