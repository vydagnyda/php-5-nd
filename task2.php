<?php
/*Duomenys gali būti iškraipyti įvairiais triukšmais. Duomenų„išlyginimo“ metu triukšmas pašalinamas tokiu būdu: kiekvienas skaičius keičiamas jo ir dviejų jam gretimų skaičių vidurkiu (vidutinės reikšmės sveikąja dalimi). Pirmas ir paskutinis skaičiai atitinkamai keičiami dviejų pirmųjų arba dviejų paskutinių skaičių vidurkiu.
•	Į įvedimo eilutę įvedama skaičių seka, atskirtų kableliais. 
•	Atfiltruokite skaičius pagal aukščiau aprašytą algoritmą ir išveskite tokią lentelę iš 2 stulpelių: skaičius, skaičius po filtravimo

TO-DO:
•	Antrojoje užduotyje gautus duomenis atvaizduokite grafikais.
•	Pirmoji kreivė turi vaizduoti duomenis prieš filtravimą
•	Antroji kreivė turi vaizduoti duomenis po filtravimo
*/
?>

<html>

<head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"> </script>
</head>

<body>
    <p>
<?php
$numbers = explode(',', $_GET['numbers']);
$filteredNumbers = [];
if (count($numbers) === 1) {
    $filteredNumbers = $numbers;
} else {
    foreach ($numbers as $key => $number) {
        if ($key === 0) { // pirmas skaičius
            $filteredNumbers[0] = round(($number + $numbers[1]) / 2);
        } elseif ($key === count($numbers) - 1) { // paskutinis skaičius
            $filteredNumbers[$key] = round(($number + $numbers[$key - 1]) / 2);
        } else {
            $filteredNumbers[$key] = round(($number + $numbers[$key - 1] + $numbers[$key + 1]) / 3);
        }
    }
}

var_dump($filteredNumbers);
?>
    </p>
    <form method="GET">
        <input name="numbers" type="text" />
        <input type="submit" value="Siųsti" />
    </form>

    <table border="1">
        <tr>
            <th>Skaičius</th>
            <th>Skaičius po filtravimo</th>
        </tr>
        <?php foreach ($numbers as $key => $number) {?>
            <tr>
            <td><?php echo $number; ?></td>
            <td><?php echo $filteredNumbers[$key] ?></td>
            </tr>
        <?php }?>


    </table>

    <canvas id="myChart"></canvas>
    <script src="filteringNumbersChart.js"></script>
    <script>
        var data = [<?php echo implode(',', $numbers); ?>];
        var labels = ['<?php echo implode("', '", array_keys($numbers)); ?>'];
        var data2 = [<?php echo implode(',', $filteredNumbers); ?>];
        initFilteringNumbersChart("myChart", labels, data, data2);
    </script>     
</body>

</html>