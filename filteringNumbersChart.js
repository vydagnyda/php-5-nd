function initFilteringNumbersChart(chartId, labels, data, data2) {
    var ctx = document.getElementById(chartId).getContext('2d');
    var chart = new Chart(ctx, {
        type: 'line',
        data: {
         labels: labels, 
         datasets: [{
             label: "Numbers before filter",
             borderColor: 'rgb(123, 99, 132)',
             lineTension: 0,
             borderWidth: 5,
             fill: false,
             data: data,
         }, {
            label: "Numbers after filter",
            borderColor: 'rgb(220,20,60)',
            lineTension: 0,
            borderWidth: 5,
            fill: false,
            data: data2,
        }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }   
    });
}
